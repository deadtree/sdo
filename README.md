# RootDO [![AUR](https://img.shields.io/aur/version/rdo.svg)](https://aur.archlinux.org/packages/rdo/)

This little "project" aims to be a very slim alternative to both sudo and doas.

### Installation

```sh
git clone https://codeberg.org/sw1tchbl4d3/rdo
cd rdo
make
sudo make install
```

After that, you'll have to configure rdo to allow you to use it.
To do this, edit `/etc/rdo.conf`, and set the username variable to your own.

After that you're good to go!

And to uninstall:
```sh
sudo make uninstall
```

### Usage

```sh
rdo [command]
```

The configuration file has the following variables:
```
username=sw1tchbl4d3
wrong_pw_sleep=1000
session_ttl=5
```

- `username`: The username of the user that is allowed to execute rdo (no multi user or group support (yet)).
- `wrong_pw_sleep`: The amount of milliseconds to sleep at a wrong password attempt. Must be a positive integer. Set to 0 to disable.
- `session_ttl`: The amount of minutes a session lasts. Must be a positive integer. Set to 0 to disable.

### Dependencies
- `libbsd`

### Benchmarks

The benchmark: Execute `whoami` (GNU coreutils 8.32) 1000 times.

|Program|Time|
--- | --- 
sudo 1.9.7p1 | 13.62s
opendoas 6.8.1 | 7.60s
rdo 1.2 | 2.25s
Baseline | 1.43s

Baseline here is how long it took without any wrapper to make it root.

These benchmarks were done on a single core of an `Intel i3-3110M` Laptop processor, on Artix Linux version `5.13.4-zen2-1-zen`.

`sudo` and `opendoas` were pulled from the pacman repos, rdo via AUR.

All configs were kept as default, except allow the `wheel` group on both + enable `persist` on doas.

Script used:
```sh
#!/bin/sh

$1 whoami

current=$(date +%s.%N)
for i in {1..1000}; do
	$1 whoami 2>&1 >/dev/null
done
done=$(date +%s.%N)

echo $done - $current | bc
```

The script requires `bc` to be installed, for floating point arithmetics.
