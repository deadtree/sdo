CFLAGS = -Wall -Wextra -Werror -Wl,-z,now
CFLAGS_RELEASE = ${CFLAGS} -O2 -s -D_FORTIFY_SOURCE=2
CFLAGS_DEBUG = ${CFLAGS} -O0 -g -fsanitize=undefined
LIBS = -lbsd -lcrypt
CC = gcc

all: sdo.c
	${CC} ${CFLAGS_RELEASE} sdo.c -o sdo ${LIBS}

debug: sdo.c
	${CC} ${CFLAGS_DEBUG} sdo.c -o sdo ${LIBS}

install: sdo
	cp sdo /usr/bin/sdo
	chown root:root /usr/bin/sdo
	chmod 755 /usr/bin/sdo
	chmod u+s /usr/bin/sdo
	cp sdo_sample.conf /etc/sdo.conf
	chmod 600 /etc/sdo.conf

uninstall:
	rm /usr/bin/sdo
	rm /etc/sdo.conf

clean:
	rm sdo
